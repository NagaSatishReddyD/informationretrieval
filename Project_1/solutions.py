import os
from os import listdir
from os.path import isfile, join
from bs4 import BeautifulSoup
import nltk
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer

"""
Write your reusable code here.
Main method stubs corresponding to each block is initialized here. Do not modify the signature of the functions already
created for you. But if necessary you can implement any number of additional functions that you might think useful to you
within this script.

Delete "Delete this block first" code stub after writing solutions to each function.

Write you code within the "WRITE YOUR CODE HERE vvvvvvvvvvvvvvvv" code stub. Variable created within this stub are just
for example to show what is expected to be returned. You CAN modify them according to your preference.
"""

def block_reader(path):
    # Delete this block first
    #raise NotImplementedError("Please implement your solution in block_reader function in solutions.py")
    # ##############
    # WRITE YOUR CODE HERE vvvvvvvvvvvvvvvv
   sgm_files = [file_entry for file_entry in listdir(path) if isfile(join(path, file_entry)) and file_entry.endswith('.sgm')]
   for entry in sgm_files:
       with open(join(path,entry)) as file:
        reuters_file_content = file.read()
        yield reuters_file_content
    # WRITE YOUR CODE HERE ^^^^^^^^^^^^^^^^

def block_document_segmenter(INPUT_STRUCTURE):
    # Delete this block first
    # raise NotImplementedError("Please implement your solution in block_document_segmenter function in solutions.py")
    # ##############
    # WRITE YOUR CODE HERE vvvvvvvvvvvvvvvv
    d = '</REUTERS>'
    for document_text in INPUT_STRUCTURE:
        article_list = [e+d for e in document_text.split(d) if len(e.strip()) > 0]
        for article in article_list:
            if '<!DOCTYPE' in article:
                article = article[article.index('>')+1:]
            yield article
    # WRITE YOUR CODE HERE ^^^^^^^^^^^^^^^^


def block_extractor(INPUT_STRUCTURE):
    # Delete this block first
    # raise NotImplementedError("Please implement your solution in block_extractor function in solutions.py")
    # ##############

    # WRITE YOUR CODE HERE vvvvvvvvvvvvvvvv
    for document_text in INPUT_STRUCTURE:
        article_soup = BeautifulSoup(document_text, 'xml')
        id = article_soup.REUTERS.get('NEWID')
        text = article_soup.get_text()
        content_dict = {"ID": id, "TEXT": text}  # Sample dictionary structure of output
        yield content_dict
    # WRITE YOUR CODE HERE ^^^^^^^^^^^^^^^^


def block_tokenizer(INPUT_STRUCTURE):
    # Delete this block first
    #raise NotImplementedError("Please implement your solution in block_tokenizer function in solutions.py")
    # ##############

    # WRITE YOUR CODE HERE vvvvvvvvvvvvvvvv
    for data in INPUT_STRUCTURE:
        id = data['ID']
        text = data['TEXT']
        for word in word_tokenize(text):
            yield (id, word)
    # WRITE YOUR CODE HERE ^^^^^^^^^^^^^^^^


def block_stemmer(INPUT_STRUCTURE):
    # Delete this block first
    # raise NotImplementedError("Please implement your solution in block_stemmer function in solutions.py")
    # ##############

    # WRITE YOUR CODE HERE vvvvvvvvvvvvvvvv
    porter_stemmer = PorterStemmer()
    for (id, token) in INPUT_STRUCTURE:
        token_stem = porter_stemmer.stem(token)
        yield (id, token_stem)
    # WRITE YOUR CODE HERE ^^^^^^^^^^^^^^^^


def block_stopwords_removal(INPUT_STRUCTURE, stopwords):
    # Delete this block first
    # raise NotImplementedError("Please implement your solution in block_stopwords_removal function in solutions.py")
    # ##############

    # WRITE YOUR CODE HERE vvvvvvvvvvvvvvvv
    if stopwords is None:
        stopwords = nltk.corpus.stopwords.words('english')
    for (id, token) in INPUT_STRUCTURE:
        if token not in stopwords:
            yield (id, token)
    # WRITE YOUR CODE HERE ^^^^^^^^^^^^^^^^